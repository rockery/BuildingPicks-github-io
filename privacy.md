**Privacy Statement**

**NOTE:**  This privacy statement complies with the California Online Privacy Protection Act (“CalOPPA”) (Bus. & Prof. Code § 22575 et seq.), concerning the collection of personally identifiable information.  This needs to be hyperlinked on your website and app in the footer under the term “Privacy” or “Privacy Policy”.

**Effective Date: January 1, 2018**

Buildingpicks, Inc. (“BPI”) collects information about you when you use BPI’s mobile applications, websites, and other online products and services (collectively, the “Services”) and through other interactions and communications you have with us.  This Privacy Statement (“Statement") applies to all persons who use our apps or Services (“Users”).

**Collection of Information**


**1. Information You Provide to Us.**  We collect information you provide directly to us.  This information may include: name, email, phone number, postal address, payment method, items requested (for delivery services), delivery notes, and other information you choose to provide.


**2. Information We Collect Through Your Use of Our Services.**  When you use our Services, we collect information about you in the following general categories:


  * Location Information: When you use the Services, we collect precise location data from information you provide and, if you permit access to location services through the permission system used by your mobile operating system (“platform”), we may also collect the precise location of your device when the app is running in the foreground or background.


  * Transaction Information: We collect transaction details related to your use of our Services, including the type of service requested, date and time the service was provided, amount charged, and other related transaction details.


  * Usage and Preference Information: We collect information about how you and site visitors interact with our Services, preferences expressed, and settings chosen. In some cases we do this through the use of cookies, pixel tags, and similar technologies that create and maintain unique identifiers.


  * Device Information: We may collect information about your mobile device, including, for example, the hardware model, operating system and version, software and file names and versions, preferred language, unique device identifier, advertising identifiers, serial number, device motion information, and mobile network information.


**3. Use of Information.**  We may use the information we collect about you to:


  * Provide, maintain, and improve our Services, including, for example, to facilitate payments, send receipts, provide products and services you request (and send related information), develop new features, provide customer support, develop safety features, authenticate users, and send product updates and administrative messages;


  * Perform internal operations, including, for example, to prevent fraud and abuse of our Services; to troubleshoot software bugs and operational problems; to conduct data analysis, testing, and research; and to monitor and analyze usage and activity trends;


**4. Sharing of Information.**  We may share the information we collect about you as described in this Statement or as described at the time of collection or sharing, including as follows:


  * With construction materials transporters to enable them to provide the Services you request;


  * With third parties to provide you a service you requested through a partnership or promotional offering made by a third party or us; 


  * With third parties with whom you choose to let us share information, for example other apps or websites that integrate with our Services, or those with a Service with which we integrate; 


  * With BPI subsidiaries and affiliated entities that provide services or conduct data processing on our behalf, or for data centralization and / or logistics purposes;


  * With vendors, consultants, marketing partners, and other service providers who need access to such information to carry out work on our behalf;


  * In response to a request for information by a competent authority if we believe disclosure is in accordance with, or is otherwise required by, any applicable law, regulation, or legal process;


  * With law enforcement officials, government authorities, or other third parties if we believe your actions are inconsistent with our User agreements, Terms of Service, or policies, or to protect the rights, property, or safety of BPI or others;


  * In connection with, or during negotiations of, any merger, sale of company assets, consolidation or restructuring, financing, or acquisition of all or a portion of our business by or into another company;


  * If we otherwise notify you and you consent to the sharing; and


  * In an aggregated and/or anonymized form which cannot reasonably be used to identify you.


**5. Your Choices.**  BPI will comply with an individual’s requests regarding access, correction, and/or deletion of the personal data it stores in accordance with applicable law.  You may opt out of receiving promotional messages from us by following the instructions in those messages. If you opt out, we may still send you non-promotional communications, such as those about Services you have requested, or our ongoing business relations.


**6. Your California Privacy Rights.**  California law permits residents of California to request certain details about how their information is shared with third parties for direct marketing purposes. BPI does not share your personally identifiable information with third parties for the third parties’ direct marketing purposes unless you provide us with consent to do so.


**7. Changes to the Statement.**  We may change this Statement from time to time. If we make significant changes in the way we treat your personal information, or to the Statement, we will provide you notice through the Services or by some other means, such as email. Your continued use of the Services after such notice constitutes your consent to the changes. We encourage you to periodically review the Statement for the latest information on our privacy practices.


**8. Contact Us.**  If you have any questions about this Privacy Statement, please contact us at support@buildingpicks.com.